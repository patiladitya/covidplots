import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from backend.rolling import *
from backend.figure import *
from apscheduler.schedulers.background import BackgroundScheduler
import atexit
import time

data = None
dist_df = None

def get_new_data():
    global data
    global dist_df
    # Parameters
    default_state = 'Maharashtra'
    default_district = 'Mumbai'
    data = update_data()
    rolling_mean(data)
    dist_df = partial_data(data, default_state, default_district)

get_new_data()

# App
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
server = app.server

app.layout = html.Div(children=[
    html.Meta(name="viewport",
    content="width=device-width, initial-scale=1"),

    html.H1(children='India Covid-19 Plots',
    style={
            'textAlign': 'center',
        }),

    dcc.Dropdown(
        id='state-dropdown',
        options=state_list(data),
        value='Maharashtra',
    ),

    dcc.Dropdown(
        id='district-dropdown',
        options=district_list(data, 'Maharashtra'),
        value='Mumbai',
    ),

    dcc.Graph(
        id='confirmed-plot',
        figure=get_figures(dist_df, 'confirmed'),
    ),

    dcc.Graph(
        id='deaths-plot',
        figure=get_figures(dist_df, 'deaths'),
    ),
])

# Callback for updating district list if State list is updated
@app.callback(
    Output('district-dropdown', 'options'),
    [Input('state-dropdown', 'value')]
)
def update_district_options(state):
    return district_list(data, state)


# Callback for updating plot if district changes
@app.callback(
    [Output('confirmed-plot', 'figure'),
    Output('deaths-plot', 'figure')],
    [Input('district-dropdown', 'value')],
    [State('state-dropdown', 'value')]
)
def update_figure(district, state):
    df = partial_data(data, state, district)
    return [get_figures(df, 'confirmed'),
            get_figures(df, 'deaths')]

if __name__ == '__main__':
    scheduler = BackgroundScheduler()
    scheduler.add_job(func=get_new_data, trigger="interval", seconds=60 * 15)
    scheduler.start()

    # Shut down the scheduler when exiting the app
    atexit.register(lambda: scheduler.shutdown())

    app.run_server(debug=True)