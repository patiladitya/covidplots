import pandas as pd
import numpy as np
import urllib.request, json
from  flatten_json import flatten_json

# Read the data
def update_data():
    url = "https://api.covid19india.org/districts_daily.json"
    with urllib.request.urlopen(url) as urlrequest:
        jsondata = json.loads(urlrequest.read().decode())

    data = pd.DataFrame(columns=[
        'state', 'district', 'confirmed', 'active', 
        'deceased', 'recovered', 'date', 'notes'])
    for statekey, state in jsondata['districtsDaily'].items():
        for districtkey, district in state.items():
            district_df = pd.json_normalize(district)
            district_df['district'] = districtkey
            district_df['state'] = statekey
            data = data.append(district_df)
    return data


def rolling_mean(data):
    # Calculate rolling averages
    districtgroups = data.groupby(['state', 'district'])
    data['daily'] = districtgroups['confirmed'].transform(lambda x: x.diff())
    data['daily_deaths'] = districtgroups['deceased'].transform(lambda x: x.diff())

    districtgroups = data.groupby(['state', 'district'])
    data['rolling_confirmed'] = districtgroups['daily'].transform(lambda x : x.rolling(window=7).mean())
    data['rolling_deaths'] = districtgroups['daily_deaths'].transform(lambda x : x.rolling(window=7).mean())


def partial_data(data, state, district):
    dist_df = data[(data['state'] == state) & (data['district'] == district)]
    return dist_df