import plotly.graph_objects as go
#import plotly.express as px

fonts = {
    'family': 'Raleway',
    'size': 14,
}

def get_figures(df, colname):
    fig = go.Figure()
    fig.update_layout(
        title=colname,
        xaxis_title="Date",
        yaxis_title="Count",
        font = {
        'family': 'Raleway',
        'size': 14,
        }
    )
    if colname == 'confirmed':
        fig.add_trace(go.Scatter(x=df['date'], y=df['rolling_confirmed'],
        mode='lines+markers', line=dict(color="blue"), textfont=fonts))
    elif colname == 'deaths':
        fig.add_trace(go.Scatter(x=df['date'], y=df['rolling_deaths'],
        mode='lines+markers', line=dict(color="red"), textfont=fonts))
    return fig

def state_list(df):
    states =  df['state'].unique().tolist()
    options = [{'label': x, 'value': x} for x in states]
    return options

def district_list(df, state):
    districts =  df[df['state'] == state]['district'].unique().tolist()
    options = [{'label': x, 'value': x} for x in districts]
    return options